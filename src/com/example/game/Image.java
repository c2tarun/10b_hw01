package com.example.game;

public class Image {
	
	private int imageId;
	private String imageName;
	private boolean imageState;	// true will represent image is selected
	
	private static final boolean SELECTED = true;
	private static final boolean NOT_SELECTED = false;
	
	public Image() {
		
	}
	
	public Image(int imageId, String imageName) {
		this.imageId = imageId;
		this.imageName = imageName;
		this.imageState = NOT_SELECTED;
	}
	
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public boolean isImageState() {
		return imageState;
	}
	public void setImageState(boolean imageState) {
		this.imageState = imageState;
	}
	
	

}
