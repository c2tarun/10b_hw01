package com.example.game;

import java.util.ArrayList;
import java.util.Random;

import com.example.hw01.R;

import android.app.Activity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

public class GameOperations {

	private ArrayList<TableRow> rows;
	private Activity mainActivity;
	private ArrayList<Image> images;
	private Random random = new Random();
	private TableLayout board;
	private final static String TAG = GameOperations.class.getSimpleName();
	private final static int NUM_OF_FRUITS = 6;

	public GameOperations(Activity mainActivity) {
		this.mainActivity = mainActivity;
		rows = new ArrayList<TableRow>();
		images = new ArrayList<Image>();
		imageViews = new ArrayList<ImageView>();
		board = (TableLayout) mainActivity.findViewById(R.id.TableLayout1);
	}

	public void init() {
		setupRows();
	}

	public void setupRows() {
		rows.add((TableRow) mainActivity.findViewById(R.id.tableRow1));
		rows.add((TableRow) mainActivity.findViewById(R.id.tableRow2));
		rows.add((TableRow) mainActivity.findViewById(R.id.tableRow3));
		rows.add((TableRow) mainActivity.findViewById(R.id.tableRow4));
		rows.add((TableRow) mainActivity.findViewById(R.id.tableRow5));

		// setup images
		images.add(new Image(R.drawable.apple, mainActivity.getResources()
				.getString(R.string.apple_name)));
		images.add(new Image(R.drawable.lemon, mainActivity.getResources()
				.getString(R.string.lemon_name)));
		images.add(new Image(R.drawable.mango, mainActivity.getResources()
				.getString(R.string.mango_name)));
		images.add(new Image(R.drawable.peach, mainActivity.getResources()
				.getString(R.string.peach_name)));
		images.add(new Image(R.drawable.strawberry, mainActivity.getResources()
				.getString(R.string.strawberry_name)));
		images.add(new Image(R.drawable.tomato, mainActivity.getResources()
				.getString(R.string.tomato_name)));
		

		for (int i = 0; i < 5; i++) {
			TableRow row = rows.get(i);
			for (int j = 0; j < 5; j++) {
				row.addView(findViewWithRandomImage());
			}
			rows.add(row);
			//board.addView(row);
			Log.i(TAG, "Row " + i + " created");
		}
		
	}

	public ImageView findViewWithRandomImage() {
		int imageChosen = findRandomLessThan(NUM_OF_FRUITS);
		ImageView view = new ImageView(board.getContext());
		view.setImageDrawable(mainActivity.getResources().getDrawable(
				images.get(imageChosen).getImageId()));
		return view;
	}

	public int findRandomLessThan(int num) {

		return random.nextInt(num);
	}

}
